export type TodoType = {
  id: number;
  title: string;
  description?: string;
  isComplete: boolean;
};
