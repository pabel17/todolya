import React from 'react';
import { StyleSheet, TextInput, TextInputProps } from 'react-native';
import { stylesGlobal, theme } from '../../theme';

const { colors } = theme;

type InputProps = TextInputProps & {
  as?: 'textArea' | 'text';
};

const Input: React.FC<InputProps> = ({
  as = 'text',
  numberOfLines = 4,
  ...rest
}) => {
  return (
    <TextInput
      placeholderTextColor={colors.text}
      style={[styles.input, as === 'textArea' && styles.inputArea]}
      multiline={as === 'textArea'}
      numberOfLines={as === 'textArea' ? numberOfLines : undefined}
      {...rest}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    borderRadius: 8,
    fontSize: 16,
    backgroundColor: 'white',
    color: colors.text,
    paddingHorizontal: 10,
    paddingVertical: 8,
    marginVertical: 10,
    ...stylesGlobal.shadow,
    ...stylesGlobal.shadowSmall,
  },
  inputArea: {
    textAlignVertical: 'top',
  },
});

export default Input;
