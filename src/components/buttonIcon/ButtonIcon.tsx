import React from 'react';
import { Pressable, PressableProps, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from '../../theme';

const { colors } = theme;
type ButtonIconProps = PressableProps & {
  size?: number;
  icon: string;
};
const ButtonAdd: React.FC<ButtonIconProps> = ({ icon, size = 40, ...rest }) => {
  return (
    <Pressable
      style={[
        styles.container,
        { width: size, height: size, borderRadius: size / 4 },
      ]}
      {...rest}>
      <Icon name={icon} size={size / 2 + 5} color="white" />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});

export default ButtonAdd;
