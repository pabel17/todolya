import React from 'react';
import { Modal, ModalProps, Pressable, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { stylesGlobal, theme } from '../../theme';

const { colors } = theme;

type ModalTodo = ModalProps & {
  children?: React.ReactNode;
};

const ModalTodo: React.FC<ModalTodo> = ({
  children,
  onRequestClose,

  ...rest
}) => {
  return (
    <Modal
      animationType="slide"
      onRequestClose={onRequestClose}
      transparent={true}
      {...rest}>
      <View style={styles.centeredView}>
        <View style={styles.centeredView}>
          <View style={[styles.modalView, stylesGlobal.shadow]}>
            <Pressable style={[styles.button]} onPress={onRequestClose}>
              <Icon name="close" size={25} color="#c7c7c7" />
            </Pressable>
            {children}
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  modalView: {
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 35,
    alignItems: 'center',
  },
  button: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },

  buttonAdd: {
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    paddingVertical: 8,
  },
});

export default ModalTodo;
