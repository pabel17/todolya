import React from 'react';
import {
  FlexStyle,
  Pressable,
  PressableProps,
  StyleSheet,
  Text,
} from 'react-native';
import { theme } from '../../theme';

const { colors } = theme;

type ButtonProps = Omit<PressableProps, 'style'> &
  FlexStyle & {
    title: string;
    type?: 'primary' | 'error';
  };

const Button: React.FC<ButtonProps> = ({
  title,
  type = 'primary',
  ...rest
}) => {
  return (
    <Pressable
      style={[
        styles.button,
        { backgroundColor: type === 'error' ? colors.error : colors.primary },
      ]}
      {...rest}>
      <Text style={[styles.text]}>{title}</Text>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    paddingVertical: 8,
  },
  text: {
    color: 'white',
    fontSize: 18,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
});

export default Button;
