import React, { useState } from 'react';
import { Pressable, StyleProp, ViewStyle } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from '../../theme';

const { colors } = theme;
type CheckboxProps = {
  color?: 'primary' | 'secondary';
  size?: number;
  style?: StyleProp<ViewStyle>;
  checked: boolean;
  onChecked?: (checked: boolean) => void;
};

const Checkbox: React.FC<CheckboxProps> = ({
  checked: checkedProp = false,
  color,
  style,
  size = 24,
  onChecked,
}) => {
  const [checked, setChecked] = useState(checkedProp);

  return (
    <Pressable
      onPress={() => {
        setChecked(!checked);
        onChecked && onChecked(!checked);
      }}
      style={style}>
      <Icon
        name={!checked ? 'checkbox-blank-outline' : 'checkbox-marked'}
        size={size}
        color={color ? colors[color] : 'black'}
      />
    </Pressable>
  );
};

export default Checkbox;
