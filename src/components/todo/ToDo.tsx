/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { Pressable, Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { useToDo } from '../../context/toDo';
import { TodoType } from '../../entities/toDo';
import { Checkbox } from '../checkbox';
import { styles } from './styles';

type ToDoProps = {
  todo: TodoType;
  onPress?: () => void;
  onComplete?: (val: boolean) => void;
};

const ToDo: React.FC<ToDoProps> = ({ todo, onPress, onComplete }) => {
  const { removeToDo: removeToDoCtx } = useToDo();

  const removeToDo = async () => {
    removeToDoCtx(todo.id!);
  };

  return (
    <View style={[styles.container, styles.inLine]}>
      <View style={[styles.content, styles.inLine]}>
        <Checkbox
          checked={todo.isComplete}
          onChecked={val => onComplete && onComplete(val)}
          color="primary"
          style={{ marginRight: 8 }}
        />
        <Pressable onPress={onPress} style={{ flex: 1 }}>
          <Text style={[styles.title, todo.isComplete && styles.textDisabled]}>
            {todo.title}
          </Text>
          <Text style={[styles.text, todo.isComplete && styles.textDisabled]}>
            {todo.description}
          </Text>
        </Pressable>
      </View>
      <Pressable style={[styles.btnDelete]} onPress={removeToDo}>
        <Icon name="close" size={20} color="white" />
      </Pressable>
    </View>
  );
};

export default ToDo;
