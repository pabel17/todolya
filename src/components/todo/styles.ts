import { StyleSheet } from 'react-native';
import { theme, stylesGlobal } from '../../theme';
const { colors } = theme;

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: 10,
    overflow: 'hidden',
    marginBottom: 12,
    marginTop: 5,
    marginHorizontal: 5,
    ...stylesGlobal.shadow,
  },
  content: {
    flex: 1,
    padding: 8,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    color: colors.secondary,
  },
  text: {
    fontSize: 16,
    color: colors.text,
  },
  btnDelete: {
    padding: 8,
    alignSelf: 'stretch',
    backgroundColor: colors.error,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inLine: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  textDisabled: {
    color: colors.textLight,
    textDecorationLine: 'line-through',
  },
});
