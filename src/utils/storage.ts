import AsyncStorage from '@react-native-async-storage/async-storage';
/**
 * @param key :string - nombre de la llave
 * @param data :data cualquier formato
 * @returns {success?, data?, error?, message?}
 */

type Response = {
  success?: boolean;
  data?: any;
  error?: any;
  message?: string;
};

export async function saveStorage(key: string, data: any): Promise<Response> {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(data));
    return { success: true };
  } catch (error) {
    return { error, message: 'Error de sintaxis' };
  }
}

export async function getStorage(key: string): Promise<Response> {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value) {
      return { success: true, data: JSON.parse(value) };
    }
    return { data: null, message: 'No existe la llave' };
  } catch (error) {
    return { error, message: 'Error al recuperar' };
  }
}

export async function clearStorage(): Promise<Response> {
  try {
    await AsyncStorage.clear();
    return { success: true };
  } catch (error) {
    return { error, message: 'No se pudo limpiar el storage' };
  }
}
