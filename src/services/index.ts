const URL = 'https://catfact.ninja/breeds';

export const getTodosService = async (cant: number) => {
  const endpoint = `${URL}?limit=${cant}`;
  const response = await fetch(endpoint);
  const res = await response.json();
  return res.data;
};
