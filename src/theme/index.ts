import { StyleSheet } from 'react-native';

const palette = {
  blue: '#4042c9',
  dark: '#0a0a0a',
  gray: '#4c4c4c',
  gray1: '#999999',
  gray2: '#dddddd',
  red: '#D21F1F',
};

export const theme = {
  colors: {
    primary: palette.blue,
    secondary: palette.gray,
    text: palette.gray1,
    textLight: palette.gray2,
    error: palette.red,
    'bg-gray1': palette.gray1,
    'bg-light': palette.gray2,
  },
};

export const stylesGlobal = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  shadowSmall: {
    elevation: 1,
  },
});
