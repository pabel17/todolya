import React, { createContext, useContext, useEffect, useState } from 'react';
import { TodoType } from '../entities/toDo';
import { saveStorage, getStorage } from '../utils/storage';

interface Context {
  todos: TodoType[];
  createTodo: (todo: Omit<TodoType, 'id'>[]) => Promise<boolean>;
  isCompleteToDo: (id: number, val: boolean) => void;
  removeToDo: (id: number) => void;
  editToDo: (todo: TodoType) => Promise<boolean>;
}

const TODO_KEY = '@todos:key';
const ToDoContext = createContext<Context>({} as Context);

export const ToDoProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [todos, setTodos] = useState<TodoType[]>([]);

  const updateTodosStorage = async (todosUpdate: TodoType[]) => {
    const res = await saveStorage(TODO_KEY, todosUpdate);
    if (res.success) {
      setTodos([...todosUpdate]);
      return true;
    } else {
      return false;
    }
  };

  const createTodo = async (todo: Omit<TodoType, 'id'>[]) => {
    const addID = todo.map((item, i) => ({
      ...item,
      id: todos.length + i + 1,
    }));
    const newTodos = [...todos, ...addID];
    return updateTodosStorage(newTodos);
  };

  const editToDo = async (todo: TodoType) => {
    const newTodos = [
      ...todos.filter(item => item.id !== todo.id),
      { ...todo },
    ];
    return updateTodosStorage(newTodos);
  };

  const removeToDo = async (id: number) => {
    const newTodos = [...todos.filter(item => item.id !== id)];
    return updateTodosStorage(newTodos);
  };

  const isCompleteToDo = (id: number, val: boolean) => {
    const newTodos = todos.map(item => {
      if (item.id === id) {
        item.isComplete = val;
      }
      return { ...item };
    });
    return updateTodosStorage(newTodos as unknown as TodoType[]);
  };

  const init = async () => {
    const res = await getStorage(TODO_KEY);
    if (res.error || res.data === null) {
      return null;
    }
    setTodos(res.data);
    console.log({ todos: res.data });
  };

  useEffect(() => {
    init();
    // clearStorage();
  }, []);

  return (
    <ToDoContext.Provider
      value={{ todos, createTodo, isCompleteToDo, removeToDo, editToDo }}>
      {children}
    </ToDoContext.Provider>
  );
};

export const useToDo = () => useContext(ToDoContext);
