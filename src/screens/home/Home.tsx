import React, { useState, useMemo, useEffect } from 'react';
import { Alert, ScrollView, StyleSheet, Text, View } from 'react-native';
import { ToDo, ModalTodo, ButtonIcon, Input } from '../../components';
import { theme } from '../../theme';
import { useToDo } from '../../context/toDo';
import { TodoType } from '../../entities/toDo';
import DialogCreateTodo from './DialogCreateTodo';
import DialogShowInfo from './DialogShowInfo';
import DialogRandomTodo from './DialogRandomTodo';
import { getTodosService } from '../../services';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const { colors } = theme;

const Home = () => {
  const [modalRandomVisible, setModalRandomVisible] = useState<boolean>(false);
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [modalVisibleInfo, setModalVisibleInfo] = useState<boolean>(false);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const [toDoInfo, setToDoInfo] = useState<TodoType>({} as TodoType);
  const { todos, createTodo, removeToDo, isCompleteToDo } = useToDo();
  const [isSearch, setIsSearch] = useState<boolean>(false);
  const [termSearch, setTermSearch] = useState('');
  const [dataFilters, setdataFilters] = useState<TodoType[]>([]);

  // return todo.title.toLowerCase().includes(termSearch.toLowerCase());
  useEffect(() => {
    console.log({ isSearch });
    if (isSearch) {
      setdataFilters(todos.filter(item => item.title.includes(termSearch)));
    } else {
      setdataFilters(todos);
    }
  }, [isSearch, termSearch, todos]);

  const addToDoGenerated = async (data: any[]) => {
    const parseTodo = data.map(item => ({
      title: item.breed,
      description: `Country: ${item.country} | Origin: ${item.origin} | Coat: ${item.coat} | Pattern: ${item.pattern} `,
      isComplete: false,
    }));

    const res = await createTodo(parseTodo);
    if (res) {
      Alert.alert('Las tareas se generaron satisfactoriamente');
    } else {
      return Alert.alert(
        'UPS! ocurrió un problema',
        'por favor vuelve a intentarlo',
      );
    }
  };

  return (
    <View style={[styles.container]}>
      <View style={[styles.header]}>
        {isSearch ? (
          <View style={{ justifyContent: 'center' }}>
            <Input autoFocus onChangeText={setTermSearch} />
            <Icon
              name="close"
              size={24}
              color={colors.text}
              style={{ position: 'absolute', right: 10, zIndex: 10 }}
              onPress={() => {
                setIsSearch(false);
                setTermSearch('');
              }}
            />
          </View>
        ) : (
          <Text style={[styles.title]}>ToDo List</Text>
        )}
        <Icon
          name="magnify"
          size={26}
          style={{ position: 'absolute', right: 15 }}
          onPress={() => setIsSearch(true)}
        />
      </View>
      <ScrollView style={[styles.container, { padding: 24 }]}>
        <View style={{ flex: 1 }}>
          {dataFilters.length > 0 ? (
            dataFilters.map((todo, i) => (
              <ToDo
                key={`${todo.id}-${i}`}
                onPress={() => {
                  setModalVisibleInfo(true);
                  setToDoInfo(todo);
                }}
                onComplete={val => {
                  isCompleteToDo(todo.id, val);
                }}
                todo={todo}
              />
            ))
          ) : (
            <Text style={[styles.label]}>
              Por el momento no tienes ningún pendiente, Por agrega una tarea!
            </Text>
          )}
        </View>
      </ScrollView>
      <View style={[styles.inLine]}>
        <ButtonIcon
          icon="repeat-variant"
          size={60}
          onPress={() => {
            setModalRandomVisible(true);
            setIsSearch(false);
          }}
        />
        <ButtonIcon
          icon="plus"
          size={60}
          onPress={() => {
            setModalVisible(true);
            setIsSearch(false);
          }}
        />
      </View>

      {/* MODAL CREATE TODO */}
      <ModalTodo
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
          setIsEditMode(false);
        }}>
        <DialogCreateTodo
          todo={toDoInfo}
          onCancel={() => {
            setModalVisible(false);
          }}
          isEditMode={isEditMode}
        />
      </ModalTodo>

      {/* MODAL SHOW INFO */}
      <ModalTodo
        visible={modalVisibleInfo}
        onRequestClose={() => {
          setModalVisibleInfo(false);
        }}>
        <DialogShowInfo
          todo={toDoInfo}
          onEdit={() => {
            setModalVisibleInfo(false);
            setIsEditMode(true);
            setModalVisible(true);
          }}
          onDelete={() => {
            removeToDo(toDoInfo.id);
            setModalVisibleInfo(false);
          }}
        />
      </ModalTodo>

      {/* MODAL RANDOM  */}
      <ModalTodo
        visible={modalRandomVisible}
        onRequestClose={() => {
          setModalRandomVisible(false);
        }}>
        <DialogRandomTodo
          onGenerate={cant => {
            getTodosService(cant).then(res => addToDoGenerated(res));
            setModalRandomVisible(false);
          }}
        />
      </ModalTodo>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 15,
  },
  label: {
    fontSize: 30,
    color: colors.textLight,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  header: {
    backgroundColor: colors.primary,
    padding: 10,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    justifyContent: 'center',
  },
  title: {
    fontSize: 30,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  inLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});

export default Home;
