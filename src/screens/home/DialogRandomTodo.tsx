import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Input } from '../../components';
import { theme } from '../../theme';

const { colors } = theme;

type DialogRandomTodoProps = {
  onGenerate?: (cant: number) => void;
};
const DialogRandomTodo: React.FC<DialogRandomTodoProps> = ({ onGenerate }) => {
  const [cant, setCant] = useState<string>('');
  return (
    <View style={{ width: '100%' }}>
      <Text style={[styles.title]}>Generar tareas </Text>
      <View>
        <Text style={[styles.label]}>Cantidad</Text>
        <Input
          autoFocus
          keyboardType="numeric"
          onChangeText={setCant}
          value={cant}
          placeholder="por Ej.: 5"
        />
      </View>
      <Button title="Generar" onPress={() => onGenerate && onGenerate(+cant)} />
    </View>
  );
};
const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    color: colors.secondary,
  },
  label: { fontSize: 20, color: colors.text },
});

export default DialogRandomTodo;
