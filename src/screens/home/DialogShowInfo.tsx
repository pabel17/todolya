import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from '../../components';
import { TodoType } from '../../entities/toDo';
import { theme } from '../../theme';

const { colors } = theme;

type DialogShowInfoProps = {
  todo: TodoType;
  onEdit?: () => void;
  onDelete?: () => void;
};
const DialogShowInfo: React.FC<DialogShowInfoProps> = ({
  todo,
  onEdit,
  onDelete,
}) => {
  return (
    <View style={{ width: '100%' }}>
      <Text style={styles.title}>{todo.title}</Text>
      <Text style={styles.label}>{todo.description}</Text>
      <Text style={[styles.label, { fontWeight: 'bold' }]}>
        Estado: {todo.isComplete ? 'tarea realizada' : 'Falta realizar'}
      </Text>
      <Button title="Editar" onPress={onEdit} marginTop={20} />
      <Button title="ELiminar" type="error" onPress={onDelete} marginTop={20} />
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: colors.secondary,
  },
  label: { fontSize: 20, color: colors.text, marginBottom: 10 },
});

export default DialogShowInfo;
