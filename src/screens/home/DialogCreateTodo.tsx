import React, { useEffect, useState } from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import { Button, Input } from '../../components';
import { useToDo } from '../../context/toDo';
import { TodoType } from '../../entities/toDo';
import { theme } from '../../theme';

const { colors } = theme;

type DialogCreateTodoProps = {
  isEditMode?: boolean;
  todo?: TodoType;
  onCancel?: () => void;
};

const DialogCreateTodo: React.FC<DialogCreateTodoProps> = ({
  isEditMode,
  todo,

  onCancel,
}) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const { createTodo, editToDo: editToDoCtx } = useToDo();

  const clearForm = () => {
    setTitle('');
    setDescription('');
  };

  useEffect(() => {
    if (isEditMode && todo) {
      setTitle(todo.title);
      setDescription(todo.description!);
    }
  }, [isEditMode, todo]);

  const addToDo = async () => {
    if (title === '') {
      Alert.alert('El titulo es obligatorio');
      return null;
    }
    const res = await createTodo([
      {
        title,
        description,
        isComplete: false,
      },
    ]);
    if (res) {
      clearForm();
      Alert.alert(title, 'se guardo satisfactoriamente');
      onCancel && onCancel();
    } else {
      return Alert.alert(
        'UPS! ocurrió un problema',
        'por favor vuelve a intentarlo',
      );
    }
  };

  const editTodo = async () => {
    const newTodo: TodoType = {
      id: todo?.id!,
      title,
      description,
      isComplete: todo?.isComplete!,
    };
    const res = await editToDoCtx(newTodo);
    if (res) {
      clearForm();
      Alert.alert(title, 'se modificó satisfactoriamente');
      onCancel && onCancel();
    } else {
      return Alert.alert(
        'UPS! ocurrió un problema',
        'por favor vuelve a intentarlo',
      );
    }
  };

  return (
    <View style={{ width: '100%' }}>
      <Text style={[styles.title]}>
        {isEditMode ? 'Edita la tarea' : 'Agrega una tarea'}
      </Text>
      <View>
        <Text style={[styles.label]}>Titulo</Text>
        <Input
          autoFocus
          onChangeText={setTitle}
          value={title}
          placeholder="Ingresa un titulo"
        />
      </View>
      <View>
        <Text style={[styles.label]}>Descripción</Text>
        <Input
          as="textArea"
          placeholder="Ingresa una descripción"
          onChangeText={setDescription}
          value={description}
        />
      </View>
      <Button
        title={isEditMode ? 'Guardar' : 'Agregar'}
        onPress={() => (isEditMode ? editTodo() : addToDo())}
        marginTop={20}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
    color: colors.secondary,
  },
  label: { fontSize: 20, color: colors.text },
  button: {
    backgroundColor: colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    paddingVertical: 8,
    width: '100%',
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
});
export default DialogCreateTodo;
