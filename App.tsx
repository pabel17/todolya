import React from 'react';
import { ToDoProvider } from './src/context/toDo';
import { Home } from './src/screens/home';

const App = () => {
  return (
    <ToDoProvider>
      <Home />
    </ToDoProvider>
  );
};

export default App;
